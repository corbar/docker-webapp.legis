#!/bin/bash

# configure the IPs for the job
# the job is run under "tomcat7" user, and the environmental variable are not available to that user
#export LOCAL_HOST_IP=`ip addr list ${NETWORK_INTERFACE} | grep "inet " | cut -d" " -f6 | cut -d"/" -f1`

if [ -z "$SPARK_MASTER_IP" ]; then
    export SPARK_MASTER_IP=`getent hosts ${SPARK_MASTER} | awk '{ print $1 }'`
fi

if [ -z "$CASSANDRA_IP" ]; then
    export CASSANDRA_IP=`getent hosts ${CASSANDRA} | awk '{ print $1 }'`
fi

#sed -e s/LOCAL_HOST_IP=.*$/LOCAL_HOST_IP=${LOCAL_HOST_IP}/ /home/spark-remote-submit.sh > /tmp1.sh
#sed -e s/SPARK_MASTER_IP=.*$/SPARK_MASTER_IP=${SPARK_MASTER_IP}/ /tmp1.sh > /tmp2.sh
#sed -e s/CASSANDRA_IP=.*$/CASSANDRA_IP=${CASSANDRA_IP}/ /tmp2.sh > /tmp.sh

#rm /tmp1.sh /tmp2.sh
#mv /tmp.sh /home/spark-remote-submit.sh
#chmod +x /home/spark-remote-submit.sh

#sed -e s/SPARK_MASTER_IP/${SPARK_MASTER_IP}/ /var/lib/tomcat7/conf/context.xml > /tmp1.xml
#sed -e s/CASSANDRA_IP/${CASSANDRA_IP}/ /tmp1.xml > /tmp2.xml
#mv /tmp2.xml /var/lib/tomcat7/conf/context.xml
#rm /tmp1.xml

# start the service
#service ${TOMCAT} start
# /usr/lib/jvm/java-8-oracle/bin/java
java -Djava.util.logging.config.file=/var/lib/${TOMCAT}/conf/logging.properties \
     -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager \
     -Djava.awt.headless=true \
     -XX:+UseConcMarkSweepGC \
     -Xms128m \
     -Xmx512m \
     -XX:MaxPermSize=300m \
     -ea \
     -Djava.endorsed.dirs=/usr/share/${TOMCAT}/endorsed \
     -classpath /usr/share/${TOMCAT}/bin/bootstrap.jar:/usr/share/${TOMCAT}/bin/tomcat-juli.jar \
     -Dcatalina.base=/var/lib/${TOMCAT} \
     -Dcatalina.home=/usr/share/${TOMCAT} \
     -Djava.io.tmpdir=/tmp/${TOMCAT}-${TOMCAT}-tmp \
      org.apache.catalina.startup.Bootstrap start >> /var/lib/${TOMCAT}/logs/catalina.out

#exec tail -f /var/log/${TOMCAT}/catalina.out

